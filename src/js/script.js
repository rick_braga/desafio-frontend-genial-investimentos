// Menu - Âncora
var $doc = $('html, body');
$('a').click(function() {
    $doc.animate({
        scrollTop: $( $.attr(this, 'href') ).offset().top
    }, 500);
    return false;
});

// Menu - Fixed
$(window).on('scroll', function() {
  var top = $(window).scrollTop();
  if (top === 0) {
    $("header").removeClass("overlay");
  } 
  else {
    $("header").addClass("overlay");
  }
});

//Menu - Responsivo
function myFunction() {
    var x = document.getElementById("myTopnav");
    if (x.className === "topnav") {
        x.className += " responsive";
    } else {
        x.className = "topnav";
    }
}

//Carrossel - Quem Somos
$('.itens').slick({
  dots: false,
  infinite: false,
  arrows:true,
  speed: 300,
  slidesToShow: 3,
  slidesToScroll: 3,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
});

//Serviços
$.getJSON('JSON/servicos.json', function(items) {
  items.forEach(function(item) {
    $('#itens').append(`<li>
                        <a href="#"><img src="../dist/img/${item.imagem}" alt=""></a>
                        <div class="caption">
                          <div class="blur"></div>
                          <div class="caption-text">
                          <h1>${item["título"]}</h1>
                          <p>${item.texto}</p>
                          </div>
                        </div>
                      </li>`)
  })
});